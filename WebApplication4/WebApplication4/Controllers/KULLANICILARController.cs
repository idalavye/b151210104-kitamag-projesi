﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication4.Models;

namespace WebApplication4.Controllers
{
    public class KULLANICILARController : Controller
    {
        private kitamagvtEntities db = new kitamagvtEntities();
       

        public ActionResult CIKIS()
        {
            Session["KULLANICIADI"] = null;
            RedirectToAction("Index", "Home");
            return View();
        }

        public ActionResult GIRIS()
        {
            return View();
        }

        
        [HttpPost]
        public ActionResult GIRIS(KULLANICILAR Model)
        {

        
        
            var KULLANICI = db.KULLANICILAR.FirstOrDefault(x => x.KULLANICIADI == Model.KULLANICIADI && x.SIFRE == Model.SIFRE);
          
            if (KULLANICI != null)
            {
                Session["KULLANICIADI"] = KULLANICI;
                return RedirectToAction("Index", "Home");
            }

            ViewBag.HATA = "Kullanıcı Adı Veya Şifre Yanlış";
            return View();
        }




        // GET: KULLANICILAR
        public ActionResult Index()
        {
            return View(db.KULLANICILAR.ToList());
        }

        // GET: KULLANICILAR/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KULLANICILAR kULLANICILAR = db.KULLANICILAR.Find(id);
            if (kULLANICILAR == null)
            {
                return HttpNotFound();
            }
            return View(kULLANICILAR);
        }

       [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        




        // POST: KULLANICILAR/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]  
        public ActionResult Create(KULLANICILAR kULLANICILAR)
        {
            if (ModelState.IsValid)
            {
                db.KULLANICILAR.Add(kULLANICILAR);
                db.SaveChanges();
                return RedirectToAction("GIRIS","KULLANICILAR");
            }

            return View(kULLANICILAR);
        }

        // GET: KULLANICILAR/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KULLANICILAR kULLANICILAR = db.KULLANICILAR.Find(id);
            if (kULLANICILAR == null)
            {
                return HttpNotFound();
            }
            return View(kULLANICILAR);
        }

        // POST: KULLANICILAR/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,KULLANICIADI,SIFRE,EMAIL")] KULLANICILAR kULLANICILAR)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kULLANICILAR).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kULLANICILAR);
        }

        // GET: KULLANICILAR/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KULLANICILAR kULLANICILAR = db.KULLANICILAR.Find(id);
            if (kULLANICILAR == null)
            {
                return HttpNotFound();
            }
            return View(kULLANICILAR);
        }

        // POST: KULLANICILAR/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            KULLANICILAR kULLANICILAR = db.KULLANICILAR.Find(id);
            db.KULLANICILAR.Remove(kULLANICILAR);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
