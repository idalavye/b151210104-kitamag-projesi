﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication4.Models;
using PagedList;

namespace WebApplication4.Controllers
{
    public class HomeController : Controller
    {
        private kitamagvtEntities db = new kitamagvtEntities();

        public ActionResult Index(string kategori, int? page)
        {

            var ogrenciler = from s in db.icerik select s;

            if (!String.IsNullOrEmpty(kategori))
            {
                int k = Int32.Parse(kategori);
                ogrenciler = ogrenciler.Where(s => s.kategori == k);

            }

            var res = ogrenciler.ToList().ToPagedList(page ?? 1, 6);

            return View(res);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}