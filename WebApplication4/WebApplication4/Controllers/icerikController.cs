﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication4.Models;
using PagedList;

namespace WebApplication4.Controllers
{
    public class icerikController : Controller
    {
        private kitamagvtEntities db = new kitamagvtEntities();

        // GET: icerik
        public ActionResult Index(int? page,String arama)
        {
            var icerikler = from s in db.icerik select s;

            if (!String.IsNullOrEmpty(arama))
            {
                icerikler = icerikler.Where(s => s.baslik.ToUpper().Contains(arama.ToUpper()));
            }

            var res = icerikler.ToList().ToPagedList(page ?? 1, 8);

            return View(res);
        }

        // GET: icerik/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            icerik icerik = db.icerik.Find(id);
            if (icerik == null)
            {
                return HttpNotFound();
            }
            return View(icerik);
        }

        // GET: icerik/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: icerik/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "icerikID,baslik,kisaaciklama,resimurl,kategori,kitapismi,yayınevi,yazar,ozet,bilgiler")] icerik icerik)
        {
            if (ModelState.IsValid)
            {
                db.icerik.Add(icerik);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(icerik);
        }

        
        // GET: icerik/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            icerik icerik = db.icerik.Find(id);
            if (icerik == null)
            {
                return HttpNotFound();
            }
            return View(icerik);
        }

        // POST: icerik/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "icerikID,baslik,kisaaciklama,resimurl,kategori,kitapismi,yayınevi,yazar,ozet,bilgiler")] icerik icerik)
        {
            if (ModelState.IsValid)
            {
                db.Entry(icerik).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(icerik);
        }

        // GET: icerik/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            icerik icerik = db.icerik.Find(id);
            if (icerik == null)
            {
                return HttpNotFound();
            }
            return View(icerik);
        }

        // POST: icerik/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            icerik icerik = db.icerik.Find(id);
            db.icerik.Remove(icerik);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
